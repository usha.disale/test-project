const fs = require('fs');
const csv = require('csv-parser');

const total = [0, 0, 0, 0, 0];
const fulldate = {};
let years = 0;
const pricnipal = {};
let year = [];
const pricnipal1 = {};
let year1 = 0;
fs.createReadStream('./public/csvFile/company_data.csv')
  .pipe(csv())
  .on('data', (result) => {
    // 1st Task on AUTHORIZED_CAP
    if (result.AUTHORIZED_CAP < 1e5) {
      total[0] += 1;
    }
    if (result.AUTHORIZED_CAP >= 1e5
        && result.AUTHORIZED_CAP < 1e6) {
      total[1] += 1;
    }
    if (result.AUTHORIZED_CAP >= 1e6
        && result.AUTHORIZED_CAP < 1e7) {
      total[2] += 1;
    }
    if (result.AUTHORIZED_CAP >= 1e8
        && result.AUTHORIZED_CAP < 1e9) {
      total[3] += 1;
    }
    if (result.AUTHORIZED_CAP >= 1e9) {
      total[4] += 1;
    }
    // 2nd Task on DATE_OF_REGISTRATION
    const dor = result.DATE_OF_REGISTRATION.split('-');
    year1 = new Date(`${dor[1]}/${dor[0]}/${dor[2]}`);
    if (dor[2] > 2000 && dor[2] <= 2018) {
      years = year1.getFullYear();
      if (!fulldate[years]) {
        fulldate[years] = 0;
      }
    }
    fulldate[years] += 1;
    // 3rd Task on PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN
    const arr = result.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;
    if (dor[2] === 2015) {
      if (!pricnipal[arr]) {
        pricnipal[arr] = 0;
      }
      pricnipal[arr] += 1;
    }
    // 4th Task on DATE_OF_REGISTRATION and PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN
    if (dor[2] >= 2010 && dor[2] <= 2018) {
      if (!pricnipal1[arr]) {
        pricnipal1[arr] = {};
        year = pricnipal1[arr];
        if (!year[dor[2]]) {
          year[dor[2]] = 1;
        }
      } else {
        year = pricnipal1[arr];
        if (!year[dor[2]]) {
          year[dor[2]] = 1;
        } else {
          year[dor[2]] += 1;
        }
      }
    }
  })
  .on('end', () => {
    fs.writeFile('public\capital_graph1.json', JSON.stringify(total))
    fs.writeFile('public\years_graph2.json', JSON.stringify(fulldate));
    fs.writeFile('public\principal_graph3.json', JSON.stringify(pricnipal));
    fs.writeFile('public\year_activity_graph4.json', JSON.stringify(pricnipal1));
  });

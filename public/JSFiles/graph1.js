fetch("capital_graph1.json")
  .then((resp) => resp.json())
  .then(function (data) {
    Highcharts.chart('container1', {
      chart:{
        type:'column'
      },
      title:{
        text:'Authorized_Capital'
      },
      xAxis:{
        categories:['<1L', '1L to 10L', '10L to 1Cr', '10Cr to 100Cr', 'More than 100Cr']
      },
      yAxis:{
        title:{
          text:'Authorized_Capital in Rupees'
        }
      },
      series:[{
        name:'Authorized_Capital',
        data:[data[0], data[1], data[2], data[3], data[4]],
        colorByPoint:true,
        dataLabels:{
          enabled:true,
          rotation:-45,
          color:'#FFFFFF',
          align:'right',
          format:'{point.y:.1f}',
          y:10,
          style:{
            fontSize:'13px',
            fontFamily:'Verdana, sans-serif',
          }
        }
      }],
    });
  });


